from sage.matrix.matrix_space import MatrixSpace
from sage.matrix.special import block_matrix
from sage.symbolic.ring import SR
from sage.graphs.graph import Graph
from sage.rings.real_mpfr import RR

# Given as input a group for which the representation theory
# primitives are defined, compute a McKay diagram
# with respect to a representation rho
def mckay_diagram(rho, undirected=False):
    G = rho.G
    try:
        irreps = G.irreducible_representations() 
    except (AttributeError, NotImplementedError):
        raise TypeError(f'group {group} does not provide a list of its representations')

    graph = Graph(multiedges=True, loops=True)
    rep_name = lambda x: str(irreps[x].dimension()) + '\'' * len([ rho for rho in irreps[:x] if rho.dimension() == irreps[x].dimension() ] )

    mult = {}
    for i, irrep1 in enumerate(irreps):
        for j, irrep2 in enumerate(irreps):
            if undirected and j >= i:
                continue
            # integer multiplicity of edge irrep1 -> irrep2
            mult[(i,j)] = RR(irrep2.dot(irrep1 * rho).real()).round()
            for _ in range(mult[(i,j)]):
                graph.add_edge(rep_name(i), rep_name(j))

    P = graph.plot()
    P.show()

    # TODO: given the multiplicities, find a nice way to draw a diagram.
    # suggest finding a longest path and then drawing branches off this
    # for an ASCII representation; or you could look into libraries
    # to draw graphs


# Would be interesting to come up with an algorithm to find nice representatives for a representation of a finitely presented group using a McKay diagram.
# For example, if we know a choice of (concrete) representation of 2 for the binary icosahedral group, using 2*2 = 3 + 1, is there a canonical way to peel off
# the trivial representation and be left with a set of matrices for 3? Then can we do the same with 3 to get 4, then 4 for 5, and so on?
# Essentially, we're trying to find a basis that is 'as block diagonal as possible' for all generators simultaneously.
# We can use the character table to find out what kind of splitting we expect to see, then must look for a basis meeting the right constraints.
# for example, if the character table tells us that 2*2 should look like 1+3, we take the three four-by-four matrices g1, g2, g3 that one gets by applying
# the naive tensor product rep of 2*2 to the generators, then looks for a matrix A such that 
# A gi A^-1 is block diagonal with blocks of size 1, 3 for all i.
# This is a sort of "representation quotient".
# Would be nice to define this quotient operation rho1 / rho2 when rho2 is irreducible and rho1 * rho2 != 0, then to extend to all subrepresentations

# looks like someone has developed an algorithm with mathematica implementation: we should check out the below:
# https://arxiv.org/pdf/2012.14440.pdf
# actually, this doesn't do what we want it to; these authors assume that irrep matrices are given to them a priori; back to the drawing board
# suppose we have a matrix M and want it in block diagonal form B = [ [B1, 0], [0, B2] ]
# then write A = [ A11, A12, A21, A22 ] and M = [ M11, M12, M21, M22 ] so A M A^-1 = B <=> [ [ A11 M11 + A12 M21, A11 M12 + A12 M22 ], [ A21 M11 + A22 M21, A12 M12 + A22 M22 ] ] = [ [ B1 A11, B1 A12 ], [ B2 A21, B2 A22 ] ]
# in other words, a system A11 M11 + A12 M21 = B1 A11
#                          A11 M12 + A12 M22 = B1 A12
#                          A21 M11 + A22 M21 = B2 A21
#                          A21 M12 + A22 M22 = B2 A22
# on its own this would not be enough to solve (underdetermined) but the point is that as we increase the number of matrices we just add new 'B' variables but not new 'A' variables
# Note we also want A, B to be unitary... This is not a flat condition and so very tricky from the algebraic solutions point-of-view
# It would be nice if we had in mind a matrix Lie group containing rho1--say, SU(N) for example.
# Then we could think of each rho1(gi) as exp(Mi) where Mi is traceless skew-hermitian and exp is the usual matrix exponential
# Now use A rho(gi) A^-1 = A exp(Mi) A^-1 = exp(A Mi A^-1) (power series representation) = exp(Ad(A)(Mi)) where Ad is the adjoint representation of U(n) on the lie algebra u(n) (containing SU(n)).
# the point is that the block-diagonality has to hold at the level of the Lie algebra, so the unitary condition can be imposed at the level of Lie algebras; instead of asking for A rho(gi) A^-1 to be unitary, we could ask for A log(rho(gi)) A^-1 to be skew hermitian.
# Presumably one can understand this condition by differentiating again to get ad(X)(Mi) = [X, Mi] = X Mi - Mi X is block diagonal, where X is a logarithm of A.
# Now the problem is starting to look much more tractable: compute logarithms M1, M2, M3 of generating matrices, and solve the system of equations [X, Mi] is block diagonal, where X is valued in skew-hermitian matrices.
# Then compute A = exp(X).
# The point is that this system of equations is fully linear in X, and so should be amenable to computer solution, unlike the unitary case.
# Now the problem moves to computing *symbolic* matrix logarithms and exponentials.
# Presumably this is not in principle difficult; for nondegenerate cases it seems reasonable to assume that the rho(gi) are at least diagonalizable, so the logarithm may be computed by diagonalizing and taking a usual logarithm.
# Diagonalization ver of log looks smth like J, P = A.jordan_form(transformation=True); P * log(J) * P.inverse() (in defining log(J) I assume it's diagonalizable so I don't have to worry about convergence issues)
# log can get very messy, so make sure to use something like logA = (P * log(J) * P.inverse()).maxima_methods().ratsimp('algebraic: true') to reduce a nicely. Generally maxima won't like to exponentiate the log though. I think it doesn't like the pi floating around
# (non-algebraic things make us sad)

# try to understand relevance of previous literature in computational representation theory to what we are doing here
# CHEVIE implements many coxeter group type things; how are we different from CHEVIE?
# this talk looks like a good introduction: http://www.math.rwth-aachen.de/~hiss/Presentations/Galway08_Lec1.pdf
# How does IrreducibleRepresentationsDixon in GAP work?

def irrep_quotient(rho1, rho2):
    if rho1.G != rho2.G:
        raise TypeError(f'representations {rho1}, {rho2}, are of different groups')
    if rho2 * rho1 == 0:
        raise "Not a subrepresentation"

    k = rho2.dimension()
    n = rho1.dimension()

    # compute a logarithm of a diagonalizable matrix
    # will satisfy e^matrix_log(M) = M, but matrix_log(M).exp() may not compute in Maxima; instead
    # one might need to compute numerically and do some kind of diophantine approximation?
    # e.g. we anticipate that any matrix A satisfying the block diagonality condition probably is
    # defined over a relatively tame number field with coefficients that aren't crazy complicated...
    def matrix_log(M):
        J, P = M.jordan_form(transformation=True)
        return (P * J * P.inverse()).maxima_methods().ratsimp('algebraic: true')

    return res, varlist
