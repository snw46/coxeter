'''
polyhedral groups...
double covers of rotation groups in SO(3)
'''

from itertools import chain, combinations

from sage.groups.free_group import FreeGroup
from sage.rings.qqbar import QQbar, AA
from sage.matrix.matrix_space import MatrixSpace
from sage.functions.other import sqrt
from sage.rings.imaginary_unit import I
from sage.rings.integer import Integer
from sage.symbolic.ring import SR
from sage.calculus.functional import expand

from .representation import trivial_representation, representation_from_generators
from .projection import C2_to_GL2, SU2_to_SO3
from .simple_geometric_group import SimpleGeometricGroup

from .utils import Progress

# just h3 for now, i don't know how the general case should work...
# TODO
# See Derek Holt's answer on MO 44631. In principle it looks like it should work
# to take a presentation of the rotary group and replace the "= 1" condition with "= n"
# where n ("= -1") is an order two element commuting with everything.
class PolyhedralGroup(SimpleGeometricGroup):
    '''
    main class implementing double covers of reflection groups, of simple type
    instantiate with a 'kind' (e.g. H3), output is the double cover of the rotation group
    of that kind (e.g. binary icosahedral group)
    '''

    def __init__(self, kind):
        # polyhedral groups are extensions of underlying rotary groups
        #RG = RotaryGroup(kind)

        # TODO: error checking
        rank = int(kind[1:])
        self.kind = kind
        free_group = FreeGroup(3)
        x1,x2,x3 = free_group.gens()

        g = x1*x2*x3
        if kind[0] == 'H':
            G = free_group.quotient([g.inverse()*x1**2, g.inverse() * x2 ** 3, g.inverse() * x3 ** 5 ])
            self._relations = [ [-3,-2,-1,1,1], [-3,-2,-1,2,2,2], [-3,-2,-1,3,3,3,3,3] ] 
        elif kind[0] == 'B':
            G = free_group.quotient([g.inverse()*x1**2, g.inverse() * x2 ** 3, g.inverse() * x3 ** 4 ])
            self._relations = [ [-3,-2,-1,1,1], [-3,-2,-1,2,2,2], [-3,-2,-1,3,3,3,3] ] 
        elif kind[0] == 'A':
            G = free_group.quotient([g.inverse()*x1**2, g.inverse() * x2 ** 3, g.inverse() * x3 ** 3 ])
            self._relations = [ [-3,-2,-1,1,1], [-3,-2,-1,2,2,2], [-3,-2,-1,3,3,3] ] 

        self._irreps = []

        super().__init__(G)

    def __repr__(self):
        return f'Order {super().order()} polyhedral group of type {self.kind}'

    def relations(self):
        return self._relations

    def su2_irrep(self, a):
        '''
        The irreducible representation of SU(2) of dimension a, restricted to this group
        '''
        # this representation is defined as acting on homogeneous polynomials of degree a-1
        # in two variables, where the action on the variables is the fundamental SU(2) rep
        # thus we have a basis x^{a-1}, x^{a-2} y, ..., x y^{a-2}, y^{a-1}.
        # now replace each x with fund(g)[0][0]*x + fund(g)[1][0] * y and each y with
        # fund(g)[0][1]*x + fund(g)[1][1]*y and expand to get the new terms.
        # So the x^{a-1} term is
        def to_matrix(g, bar, gen_num):
            fund = self.fundamental_representation()
            x, y = SR.var('x'), SR.var('y')
            A = fund(g)[0][0] * x + fund(g)[1][0] * y
            B = fund(g)[0][1] * x + fund(g)[1][1] * y
            MS = MatrixSpace(QQbar, a)
            vecs = []
            

            offset = float(gen_num)/(len(self.gens()))
            interv= float(1)/len(self.gens())

            def powerset(iterable):
                "powerset([1,2,3]) → () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
                s = list(iterable)
                return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


            for deg in range(a):
                bar.progress(offset + interv * float(deg)/a)
                vec = [ 0 for _ in range(a) ]

                # compute a manual expansion of A ** deg * B ** ((a - 1) * deg)
                for i, take_x_set in enumerate(powerset(range(a-1))):
                    bar.progress(offset + interv * (float(deg)/a + 1.0/a * float(i) / pow(2.0, a-1)))
                    contrib = 1
                    for j in range(a-1):
                        if j < deg:
                            if j not in take_x_set:
                                contrib *= fund(g)[1][0]
                            else:
                                contrib *= fund(g)[0][0]
                        else:
                            if j not in take_x_set:
                                contrib *= fund(g)[1][1]
                            else:
                                contrib *= fund(g)[0][1]

                    vec[len(take_x_set)] += contrib

                vecs.append(vec)

            return MS.matrix(vecs).conjugate().transpose()

        with Progress(f'compute res of {a}') as bar:
            gens = { g: to_matrix(g, bar, i) for i, g in enumerate(self.gens()) }
        return representation_from_generators(self, gens, base_ring=QQbar)

    def irreducible_representations(self, numeric=False):
        '''
        compute irreducible representations of a polyhedral group
        '''

        if len(self._irreps) > 0:
            return self._irreps

        # start with a trivial and fundamental representation
        self._irreps.append(trivial_representation(self, MatrixSpace(AA,1)))
        MS = MatrixSpace(QQbar, 2)
        gens = self.gens()

        One = Integer(1)
        if self.kind[0] == 'H':
            tau = (sqrt(5) + Integer(1))/2
            self._irreps.append(representation_from_generators(
                self,
                { gens[0] : Integer(1)/2 * MS.matrix([ [I/tau, tau + I], [ -tau + I, -I/tau ] ]),
                  gens[1] : Integer(1)/2 * MS.matrix([ [ 1 + I, 1 + I ], [ -1 + I, 1 - I ] ]),
                  gens[2] : Integer(1)/2 * MS.matrix([ [ tau + I/tau, One ], [-One, tau - I/tau ] ]) },
                base_ring=QQbar))
        elif self.kind[0] == 'B':
            # quaternion generators for BO are i, j, (1 + i + j + k)/2
            sqrt2 = sqrt(Integer(2))
            self._irreps.append(representation_from_generators(
                self,
                { gens[0] : One/sqrt2 * C2_to_GL2([ I, 1 ]),
                  gens[1] : One/2 * C2_to_GL2([ 1 + I, 1 + I ]),
                  gens[2] : One/sqrt2 * C2_to_GL2([ 1 + I, 0 ])},
                base_ring=QQbar))

        # add all other restricted representations
        a = 2
        while True:
            a += 1
            restricted = self.su2_irrep(a)
            if (restricted.dot(restricted)).norm() > 1.5:
                break
            self._irreps.append(restricted)


        # there should be a better way to do this.
        # note 2p and 3p come from a Galois automorphism, and 4p a tensor product
        # we should be able to code this directly

        if self.kind[0] == 'H':
            # 2P
            taup = -2/(sqrt(5) + Integer(1))
            self._irreps.append(representation_from_generators(
                self,
                { gens[0] : Integer(1)/2 * MS.matrix([ [I/taup, taup + I], [ -taup + I, -I/taup ] ]),
                 gens[1] : Integer(1)/2 * MS.matrix([ [ 1 + I, 1 + I ], [ -1 + I, 1 - I ] ]),
                 gens[2] : Integer(1)/2 * MS.matrix([ [ taup + I/taup, One ], [-One, taup - I/taup ] ]) },
                base_ring=QQbar))

            # 4P = 2P * 2
            self._irreps.append(self._irreps[-1] * self._irreps[1])

            # 3P
            self._irreps.append(representation_from_generators(self,
                { g : SU2_to_SO3(self._irreps[-2](g)) for g in gens }, base_ring=AA))
        elif self.kind[0] == 'B':
            MS = MatrixSpace(QQbar, 1)
            self._irreps.append(representation_from_generators(
                self,
                { gens[0]: MS.matrix([-1]),
                  gens[1]: MS.matrix([1]),
                  gens[2]: MS.matrix([-1]) },
                base_ring=QQbar))

            sgn = self._irreps[-1]
            self._irreps.append(self._irreps[1] * sgn)
            self._irreps.append(self._irreps[2] * sgn)

            # still missing a 2d real rep
            # this rep is lifted from the 2d real rep of S4
            # the S4 rep has trace 2 on double transpositions, 0 on (12) and (1234) and -1 on (123)
            # in the nlab notation, gens r1 r2 r3 of BO are resp. of types g, a, e.
            # thus should have traces 0, -1, 0; i.e. shuold project to cycle types (12), (123), (1234)
            # all respectively
            # can use the Young theory to work out s4 rep: see https://arxiv.org/pdf/1112.0687
            print("warning: real 2 dimensional representation does not yet have unitary matrices")
            MS = MatrixSpace(QQbar, 2)
            M12 = MS.matrix([ [ 1, 0 ], [ -1, -1 ] ])
            M23 = MS.matrix([ [ 0, 1 ], [  1,  0 ] ])
            M34 = MS.matrix([ [ 1, 0 ], [ -1, -1 ] ])
            M123 = M12 * M23
            M1234 = M123 * M34
            self._irreps.append(representation_from_generators(
                self,
                { gens[0]: M23,
                  gens[1]: M123,
                  gens[2]: M1234 },
                base_ring=QQbar))


        return self._irreps

    def fundamental_representation(self):
        return self.irreducible_representation(1)
