from coxeter.polytope import OrbitPolytope
from coxeter.polyhedral_group import PolyhedralGroup
from coxeter.projection import stereo

BI = PolyhedralGroup("H3")
fund = BI.irreducible_representations()[1]
P = OrbitPolytope(fund, [1, 0]) 
pp = P.plot(projection=stereo,skeleton_depth=1,antialiased=True, color=(0,0,0))
pp.show()
