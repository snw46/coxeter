'''
A class for working with convex polytopes
'''

import logging
from functools import reduce

import operator as op
import numpy as np
import matplotlib.pyplot as plt

from sage.modules.free_module import VectorSpace
from sage.rings.qqbar import QQbar, AA
from sage.rings.cc import CC
from sage.rings.real_mpfr import RR

from scipy.spatial._qhull import _Qhull

from .projection import keep, approx, C_to_R

logger = logging.getLogger(__name__)
epsilon = 1e-9

class Polytope():
    '''
    A class for working with convex polytopes
    '''

    def __init__(self, _realization, _base_ring, _vertices=None, _dimension = None):
        logger.info(f'computing polytope from realization, dimension = {_dimension}')
        self._realization = _realization
        self._base_ring = _base_ring

        if self._base_ring == QQbar or self._base_ring == CC:
            raise ValueError(f'received complex base ring {_base_ring}; must convert to real first')

        # make sure the input is full-dimensional
        def shred(pts, force=False):
            '''
            delete the index from pts for which the range is minimized.
            Throws ValueError when this range is not zero, unless force is set to True
            '''
            dim = len(pts[0])
            shred_coord = min(range(dim),
                              key=lambda i: max(pt[i] for pt in pts) - min(pt[i] for pt in pts))
            shred_val = max(pt[shred_coord] for pt in pts) - min(pt[shred_coord] for pt in pts)

            logger.info(f'smallest coordinate difference is {shred_val}')

            if not force and shred_val > epsilon:
                raise ValueError('data are full dimensional and cannot be shredded')
            return [ pt[:shred_coord] + pt[shred_coord+1:] for pt in pts ]

        if _dimension is None:
            try:
                logger.info(f'start shredding, polytope is {len(self._realization[0])}-dimensional')
                while True:
                    _realization = shred(_realization)
            except ValueError:
                _dimension = len(self._realization[0])
                logger.info(f'after shredding, polytope is {_dimension}-dimensional')

        self._dimension = _dimension
        if _vertices is None:
            _vertices = list(range(len(_realization)))
        self._vertices = _vertices
        self._facets = []

        if _dimension > 1:
            # Qhull does not appreciate being fed points that are not full-dimensional
            # to remedy this, we should shred dimensions from the points until they are the proper
            # dimension. the algorithm to do this is to find the coordinate that varies the least
            # and delete it (this preserves the topology;
            # the geometry is contained in the realization)

            # match by ring and automatically convert to a real floating point number
            logger.info(f'shredding points to desired dimension {_dimension}')
            points = [ _realization[v] for v in _vertices ]
            while len(points[0]) > _dimension:
                points = shred(points, force=True)
            points = np.ascontiguousarray(points, dtype=np.double)

            logger.info('computing facets with qhull')
            qhull = _Qhull(b"i", points, b"Qc")
            facets, _ = qhull.get_hull_facets()

            # the facet f is the indices inside the realized points, which means we have to map
            # *back* the vertex function
            vx_reverse_map = dict(enumerate(_vertices))
            for f in facets:
                self._facets.append(Polytope(_realization,
                                             _base_ring,
                                             _vertices=sorted([ vx_reverse_map[i] for i in f ]),
                                             _dimension=_dimension-1))

    def __eq__(self, other):
        # checking equality in memory
        if self._realization is not other._realization:
            return False

        return self._vertices == other._vertices

    # get the key'th vertex of the polytope
    def __getitem__(self, key):
        return self._realization[self._vertices[key]]

    def __add__(self, other):
        if self._base_ring != other._base_ring:
            raise ValueError(f'Cannot add polytopes over different base rings ({self._base_ring} and {other._base_ring}')
        return Polytope(self._realization + other._realization, self._base_ring)

    def __repr__(self):
        if self._dimension == 0:
            return f'vertex {self.__getitem__(0)}'

        if self._dimension == 1:
            return f'edge {self.__getitem__(0)} --- {self.__getitem__(1)}'

        return f'{self._dimension}-dimensional polytope with {len(self._vertices)} vertices'

    def base_ring(self):
        return self._base_ring

    def cells(self, k):
        '''
        cells should return a list of the dimension k cells of this complex
        e.g. n => [self], n-1 => self.cell, n-2 => union of all self.cell[i].cells(n-2), etc.
        '''
        if k == 0:
            return [Polytope(self._realization, self._base_ring, _vertices=[v], _dimension=0) for v in self._vertices]

        if k == self._dimension:
            return [self]

        # helper to get unique elements from a list
        def unique(ls):
            unique_list = []
            for x in ls:
                if op.countOf(unique_list, x) == 0:
                    unique_list.append(x)
            return unique_list

        cell_list = []
        for cell in self._facets:
            cell_list.extend(cell.cells(k))

        return unique(cell_list)


    def plot(self, projection=keep([0,1,2]), skeleton_depth=1, **kwargs):
        return PolytopePlotter(self, projection, skeleton_depth, **kwargs)

class OrbitPolytope(Polytope):
    '''
    Define a polytope via the Coxeter construction, whose vertices are
    determined by a (complex) representation of a group G
    '''
    def __init__(self, rho, v):
        # convert v to an element of the proper vector space
        v = VectorSpace(rho.base_ring(), rho.dimension())(v)
        if rho.base_ring() == QQbar or rho.base_ring() == CC:
            vertices = [ C_to_R(list(rho(g) * v)) for g in rho.G ]
        else:
            vertices = [ list(rho(g) * v) for g in rho.G ]

        if rho.base_ring() == QQbar or rho.base_ring() == AA:
            super().__init__(vertices, AA)
        elif rho.base_ring() == CC or rho.base_ring() == RR:
            super().__init__(vertices, RR)
        else:
            raise ValueError(f'Unrecognized base ring {rho.base_ring()}')

class PolytopePlotter:
    def __init__(self, polytope, projection=keep([1,2,3]), skeleton_depth=1, **kwargs):
        self.polytope = polytope
        ax = plt.figure().add_subplot(projection='3d')
        ax.set_aspect('equal')
        ax.set_axis_off()

        def plot_edge(ax, cell, kwargs):
            a = projection(approx(cell[0]))
            b = projection(approx(cell[1]))
            ax.plot(*zip(a,b), **kwargs)

        def plot_poly(ax, cell, kwargs):
            vxs_unsort = [ projection(approx(v[0])) for v in cell.cells(0) ]
            vxs = [ vxs_unsort[0] ]
            del vxs_unsort[0]
            while len(vxs_unsort) > 0:
                best_dist = 1e9
                best_vert = None
                dist = lambda x,y: (x[0]-y[0])**2 + (x[1]-y[1])**2 + (x[2]-y[2])**2
                for i, v in enumerate(vxs_unsort):
                    if dist(v, vxs[-1]) < best_dist:
                        best_dist = dist(v,vxs[-1])
                        best_vert = i
                vxs.append(vxs_unsort[best_vert])
                del vxs_unsort[best_vert]

            xs = []
            ys = []
            zs = []
            tris = []
            for i, pt in enumerate(vxs):
                xs.append(pt[0])
                ys.append(pt[1])
                zs.append(pt[2])
                if i >= 2:
                    tris.append([0, i-1, i])

            col_dict = { 1: (1,0,0), 2 : (0,0,1), 3 : (0, 1, 0), 4 : (1, 1, 0), 5 : (0, 1, 1), 6 : (1, 0, 1) }
            ax.plot_trisurf(xs, ys, zs, triangles=tris, color=col_dict[len(tris)], **kwargs)

        # edge-mode
        if skeleton_depth == 1:
            for cell in polytope.cells(1):
                plot_edge(ax, cell, kwargs)

        # mesh-mode
        if skeleton_depth == 2:
            for cell in polytope.cells(2):
                plot_poly(ax, cell, kwargs)

    def show(self):
        plt.show()
