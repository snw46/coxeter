'''
Projections for use with the polytope module
'''

from sage.algebras.quatalg.quaternion_algebra import QuaternionAlgebra
from sage.symbolic.ring import SR
from sage.rings.cc import CC
from sage.rings.real_mpfr import RR
from sage.rings.qqbar import QQbar, AA
from sage.rings.imaginary_unit import I
from sage.rings.integer import Integer
from sage.modules.free_module import VectorSpace
from sage.matrix.matrix_space import MatrixSpace
from sage.matrix.special import block_matrix

def forget(ind):
    '''
    project a vector from R^n to R^n-1 by deleting an index
    '''
    def proj(xs):
        return xs[:ind] + xs[ind+1:]
    return proj

def keep(inds):
    '''
    keep only the indicated indices
    '''
    def proj(xs):
        return [ xs[i] for i in inds ]
    return proj

def C_to_R(xs, numeric=False):
    '''
    the standard projection C^n -> R^2n
    converts symbolic expressions to floating-point values
    (z1,..,zn) -> (re(z1), im(z1), ..., re(zn), im(zn))
    '''
    ring = RR if numeric else AA

    ans = []
    for x in xs:
        ans.append(ring(x.real()))
        ans.append(ring(x.imag()))

    return ans

def C_to_R_mtx(m, numeric=False):
    ring = RR if numeric else AA
    MSn = MatrixSpace(ring, m.ncols())
    m_re = MSn.matrix([ [ x.real() for x in y ] for y in m ])
    m_im = MSn.matrix([ [ x.imag() for x in y ] for y in m ])
    MS2n = MatrixSpace(ring, 2*m.ncols())
    return MS2n.matrix(block_matrix([ [m_re, m_im], [-m_im, m_re] ], subdivide=False))

# stereographic projection through the first coordinate
# send [-1,0,0,0] to 0, [0,x,y,z] to [x,y,z]
def stereo(xs):
    return [ x / (Integer(1) - xs[0]) for x in xs[1:] ]

# convert input to a real floating-point vector.
def approx(xs):
    if len(xs) == 0:
        return []
    return [ RR(x) for x in xs ]

# the standard map C2 -> GL(C2) that maps
# the unit sphere to SU(C2)
def C2_to_GL2(xs):
    MS = MatrixSpace(QQbar, 2)
    return MS.matrix([[xs[0], xs[1]], [-xs[1].conjugate(), xs[0].conjugate() ]])

def SU2_to_SO3(M, numeric=False):
    FC = CC if numeric else QQbar
    FR = RR if numeric else AA
    VS = VectorSpace(FC, 2)
    MS = MatrixSpace(FC, 2)
    MSR = MatrixSpace(FR, 3)
    M = MS.matrix(M)

    Q = QuaternionAlgebra(SR, -1, -1)
    i, j, k = Q.gens()
    v = M[0][0].real() * Q.one() + M[0][0].imag() * i + M[0][1].real() * j + M[0][1].imag() * k
    return MSR.matrix([ list(v * i * v.conjugate())[1:],
                        list(v * j * v.conjugate())[1:],
                        list(v * k * v.conjugate())[1:] ]).transpose()
