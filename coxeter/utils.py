import sys

# small wrapper for a progress bar. UB to have two going at once
class Progress():
    def __init__(self, desc, bar_len = 20):
        self.desc = desc
        self.bar_len = bar_len 

    def __enter__(self):
        self.progress(0)
        return self

    def progress(self, percent):
        percent = float(percent)
        progress_line = "=" * round(self.bar_len * percent)
        progress_line += " " * (self.bar_len - len(progress_line))
        completion_percent = min(100, round(percent * 100))
        sys.stdout.write("\r")
        sys.stdout.write(f'{self.desc} [{progress_line}] ({completion_percent}%)')
        sys.stdout.flush()

    def __exit__(self, *_):
        self.progress(1)
        sys.stdout.write('  done! \\(^^)/\n')
        sys.stdout.flush()
