from coxeter.polyhedral_group import PolyhedralGroup

G = PolyhedralGroup("H3")

one, two, three, four, five, six, twop, fourp, threep = G.irreducible_representations()
