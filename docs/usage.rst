Usage
=====

Some useful examples are given in the ``examples`` directory.
Here's some of the cool things you can do with ``coxeter``.

We can make orbit polytopes easily;

::

    from coxeter.polytope import *
    from coxeter.polyhedral_group import PolyhedralGroup

    BI = PolyhedralGroup("H3")
    fund = BI.fundamental_representation()
    P = OrbitPolytope(fund)
    pp = P.plot()
    pp.show()

... with all sorts of projections!

::

    from coxeter.polytope import OrbitPolytope
    from coxeter.polyhedral_group import PolyhedralGroup
    from coxeter.projection import stereo

    BI = PolyhedralGroup("H3")
    fund = BI.fundamental_representation()
    P = OrbitPolytope(fund, [1, 0])
    pp = P.plot(projection=stereo,skeleton_depth=1,antialiased=True, color=(0,0,0))
    pp.show()

Or even unions of polyhedra, with faces included!

::

    from coxeter.polytope import *
    from coxeter.polyhedral_group import PolyhedralGroup

    BI = PolyhedralGroup("H3")
    three = BI.su2_irrep(3) # or three = BI.irreducible_representation(2)

    def normalize(v):
        lenv = v[0] ** 2 + v[1] ** 2 + v[2] ** 2
        return [ x / sqrt(lenv) for x in v ]

    P = OrbitPolytope(three, normalize([1, 1,0])) + OrbitPolytope(three, normalize([1,0,1])) + OrbitPolytope(three, normalize([0,1,0]))
    pp = P.plot(skeleton_depth=2,antialiased=True)
    pp.show()

We can compute restricted representations, and their decompositions;

::

    from coxeter.polyhedral_group import PolyhedralGroup

    G = PolyhedralGroup("H3")
    dim = 7
    rho = G.su2_irrep(dim)
    print(f"The {dim} dimensional irrep of SU2, restricted to {G}, has...")
    for irrep in G.irreducible_representations():
        print("{rho.dot(irrep)} copies of {repr(irrep)}")

... and draw McKay diagrams.

::

    from coxeter.polyhedral_group import PolyhedralGroup
    from coxeter.mckay import mckay_diagram

    mckay_diagram(PolyhedralGroup("B3").fundamental_representation())

Representations can be evaluated on group elements.

::

    from coxeter.polyhedral_group import PolyhedralGroup
 
    G = PolyhedralGroup("H3")

    # the tensor product of 2 and 3
    rho = G.irreducible_representation(1) * G.irreducible_representation(2)

    print(*[ rho(g) for g in G.gens() ])

We can compute canonical transforms;

::

    from coxeter.polyhedral_group import PolyhedralGroup
    from coxeter.representation import canonical_transform

    two = G.irreducible_representation(1)
    four = G.irreducible_representation(3)
    rho = two * four

    P = canonical_transform(rho)
    print( P.inverse() * rho(G.gens(0)) * P )

... and use them to compute equivariant maps.

::

    from coxeter.polyhedral_group import PolyhedralGroup
    from coxeter.representation import canonical_transform

    two = G.irreducible_representation(1)
    three = G.irreducible_representation(2)
    four = G.irreducible_representation(3)

    print(compute_equivariant_map(two * three, four))
