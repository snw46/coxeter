from sage.matrix.special import block_matrix
from sage.matrix.matrix_space import MatrixSpace
from .representation import ring_tensor

def canonical_transform(V, reps=None):
    '''
    put a representation V into canonical form
    pass a list reps of representations to override
    the default ordering of blocks
    we use the algorithm of Bischer Doring and Trautner (2020)
    If P is the output of this routine, then P.inverse() * V(g) * P
    will be in block form for all g
    '''
    if reps is None:
        reps = V.G.irreducible_representations()

    vec = []
    for rho in reps:
        if V.dot(rho) == 0:
            continue

        M = block_matrix([ [rho(V.G.e).tensor_product(V(g)) -
                                rho(g).transpose().tensor_product(V(V.G.e))]
                            for g in V.G.gens() ], subdivide=False)
        for v in M.right_kernel().basis():
            vec.extend(list(v))

    MS = MatrixSpace(V.base_ring(), V.dimension())
    return MS.matrix(vec).transpose()

def compute_equivariant_map_2(V, W):
    '''
    It is helpful, frequently to be able to compute a non-zero
    equivariant map between two representations. That is, a matrix
    M not equal to zero such that for every generator g of G (and thus
    for all g in G) it holds that M * V(g) = W(g) * M
    (It is an a priori requirement that if W is k x k and
    V is n x n then M must be k x n, and the LHS/RHS are k x n)
    This function computes such an equivariant map in the event it exists,
    or returns None otherwise
    '''
    if V.G != W.G:
        raise TypeError(f'representations {V}, {W} have different base group')

    # if V, W are orthogonal there is no equivariant map between them
    if V.dot(W) == 0:
        return None

    # otherwise, we can select a single irreducible representation rho that is
    # a subrepresentation of both V and W. We put both V, W in canonical form
    # with the block corresponding to rho first, and use an identity matrix
    # transform
    irrep_list = None
    for i, rho in enumerate(V.G.irreducible_representations()):
        if V.dot(rho) != 0 and W.dot(rho) != 0:
            irrep_list = V.G.irreducible_representations()[i:] + \
                            V.G.irreducible_representations()[:i]
            break

    P, Q = canonical_transform(V, irrep_list), canonical_transform(W, irrep_list)
    MS = MatrixSpace(ring_tensor(V.base_ring(), W.base_ring()), W.dimension(), V.dimension())
    canonical_map = MS.matrix(lambda i, j: 1 if i == j and i < irrep_list[0].dimension() else 0)
    return Q * canonical_map * P.inverse()

# As i first implemented it, the solving is very slow
# Slower, perhaps, than it needs to be; if we know all the representations
# we can use builtin similarity transform algorithms to block-diagonalize,
# apply Schur's lemma, then transform backward accordingly...
def compute_equivariant_map(V, W):
    from sage.symbolic.relation import solve
    from sage.rings.integer import Integer 
    from sage.matrix.special import identity_matrix, zero_matrix, block_diagonal_matrix

    G = V.G
    if V.G != W.G:
        raise TypeError(f'representations {V}, {W} have different base group')

    v_canon_factors = []
    w_canon_factors = []
    canon_mat_block = []
    for rho in G.irreducible_representations():
        vmul = Integer(rho.dot(V).real())
        wmul = Integer(rho.dot(W).real())
        v_canon_factors.extend([rho] * vmul)
        w_canon_factors.extend([rho] * wmul)
        if min(vmul, wmul) > 0:
            canon_mat_block.append(identity_matrix(QQbar, rho.dimension() * min(vmul,wmul)))
        if min(vmul, wmul) < max(vmul, wmul):
            canon_mat_block.append(zero_matrix(QQbar, rho.dimension() * (max(vmul,wmul) - min(vmul,wmul))))

    canon_mat = block_diagonal_matrix(canon_mat_block)
    v_canon = reduce(lambda a, b: a + b, v_canon_factors)
    w_canon = reduce(lambda a, b: a + b, w_canon_factors)
   
   # TODO: This does not work. Perhaps we want to implement the 2020 paper's algorithm here?
   # looking for an algorithm performing a simultaneous block diagonalization to the form v_canon
   # (should work for all V(G.gen(i)) simultaneously...)
    _, Pv = v_canon(G.gen(0)).is_similar(V(G.gen(0)), transformation=True)
    _, Pw = v_canon(G.gen(0)).is_similar(V(G.gen(0)), transformation=True)

    # As a postcondition, suppose Pv * V * ~Pv is block diagonal and Pw * W * ~Pw is block diagonal.
    # Then M = canon_mat satisfies  M Pv * V * ~Pv = Pw * W * ~Pw * M 
    # so ~Pw M Pv * V = W * ~Pw M Pv, and ~Pw M Pv is our desired equivariant map between the two matrices
    return Pw.inverse() * canon_mat * Pv
    '''
    # Construct a matrix of variables
    MS = MatrixSpace(SR, W.dimension(), V.dimension())
    MSV = MatrixSpace(SR, V.dimension(), V.dimension())
    MSW = MatrixSpace(SR, W.dimension(), W.dimension())
    M = MS.matrix(lambda i,j : SR.var(f'x_{i}_{j}'))
    print(M)
    # Construct the equations satisfied by these variables
    eqns = []
    for g in G.gens():
        equation_matrix = M * MSV.matrix(V(g)) - MSW.matrix(W(g)) * M
        for row in equation_matrix:
            eqns.extend(row)

    print(eqns)
    # Solve equations
    for s in solve(eqns, M.list()):
        Msol = M.subs(s)
        return Msol
    '''

def print_character_table(G):
    '''
    print the character table of a group G
    G should implement conjugacy_classes() and irreps()
    '''
    sep = '\t\t'
    line = ['']
    for cc in G.conjugacy_classes():
        line.append(f'{cc.representative().order()}')
    print(sep.join(line))

    line = ['']
    for cc in G.conjugacy_classes():
        line.append(f'(size {len(cc)})')
    print('\t'.join(line))

    for rho in G.irreps():
        line = [str(rho.dimension())]
        for cc in G.conjugacy_classes():
            line.append(f'{rho(cc.representative()).trace()}')
        print(sep.join(line))
