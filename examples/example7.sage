from coxeter.polyhedral_group import PolyhedralGroup
from coxeter.representation import canonical_transform

G = PolyhedralGroup("H3")
fund = G.irreducible_representations()[1]
four = fund * fund
three = G.irreducible_representations()[4]
triv = G.irreducible_representations()[0]

can = triv + three
