from coxeter.polytope import *
from coxeter.polyhedral_group import PolyhedralGroup

BI = PolyhedralGroup("H3")
three = BI.irreducible_representations()[4]

def normalize(v):
    lenv = v[0] ** 2 + v[1] ** 2 + v[2] ** 2
    return [ x / sqrt(lenv) for x in v ]

P = OrbitPolytope(three, normalize([1,1,0]))+ OrbitPolytope(three, normalize([1,0,1])) + OrbitPolytope(three, normalize([0,1,0])) + OrbitPolytope(three, normalize([1,1,1]))
pp = P.plot(skeleton_depth=2,antialiased=True)
pp.show()
