'''
A simple geometric group abstracts over reflection groups, rotary subgroups, and double covers.
This class implements common group wrapper methods for the three; it is not intended to be
instantiated on its own. note this is NOT intended to store direct product groups for example
(semisimple types) The reason is that GAPs builtin methods for conjugacy class computations and
other representation theoretic stuff on products is very slow; we can save ourselves clock time
if we abstract over these methods and use some a priori rep theory knowledge on the backend
'''

# experimental
from functools import lru_cache

# TODO: instantiate as ABC and ask for irreducible_representations()?
class SimpleGeometricGroup():
    '''
    SimpleGeometricGroup wraps an underlying GAP group G.
    It abstracts over behaviours that need to be modified to do some representation theory
    Every underlying group should be a quotient of a free group
    '''

    def __init__(self, G):
        self.G = G
        self.e = self.G.gens()[0] ** 0

    def __eq__(self, other):
        return self.G == other.G

    def __len__(self):
        return len(self.G)

    def __hash__(self):
        return 17 * self.G.__hash__()

    def one():
        '''
        return the neutral element of the group
        '''
        return self.e

    def gap(self):
        '''
        return the underlying sage-wrapped GAP group object
        '''
        return self.G

    def id(self):
        '''
        return identity element of group
        '''
        return self.e

    def order(self):
        '''
        return the order of the group
        '''
        return self.G.order()

    def __iter__(self):
        return self.G.__iter__()

    # return 'key'th element
    def __getitem__(self, key):
        try:
            return self.G[key]
        except Exception as exc:
            raise KeyError from exc

    def gen(self, key):
        '''
        return the key'th generator
        '''
        return self.G.gen(key)

    def gens(self):
        '''
        return a list of generators of the underlying GAP group object
        '''
        return self.G.gens()

    def conjugacy_classes(self):
        '''
        return a list of GAP conjugacy classes of the underlying group
        '''
        return self.G.conjugacy_classes()

    # Put a word in 'standard form'; i.e. in list(self.G)
    # TODO: should this be LRU cached?
    @lru_cache(maxsize=None)
    def reduce_word(self, word):
        '''
        rewrite a word as an element obtainable from the __getitem__ method
        '''
        try:
            if word.parent() != self.G:
                raise TypeError(f'word {word} is not an element of group {self.G}')
        except AttributeError:
            raise TypeError(f'word {word} is not a GAP group element')

        for g in self.G:
            if g == word:
                return g

        raise RuntimeError(f'word {word} could not be reduced in group {self.G}')

    def conjugate_word(self, word):
        '''
        rewrite a word as a conjugate word appearing as a cc representative
        '''
        try:
            if word.parent() != self.G:
                raise TypeError(f'word {word} is not an element of group {self.G}')
        except AttributeError:
            raise TypeError(f'word {word} is not a GAP group element')

        for cc in self.G.conjugacy_classes():
            if word in cc:
                return cc.representative()

        raise RuntimeError(f'word {word} had no conjugate representative in {self.G}')

    def _character_orthogonality_test(self, complete=False):
        '''
        check that all irreducible representations of the group are orthogonal
        if complete=True is provided, check that the group has all irreducible representations
        computed by the sum of squares formula.
        '''
        try:
            irrep = self.irreducible_representations()
        except AttributeError as exc:
            raise exc

        for i, x in enumerate(irrep):
            if x * x != 1:
                return False
            for j in range(i):
                if x * irrep[j] != 0:
                    return False

        if complete and sum(x.dimension() ** 2 for x in irrep) != self.order():
            raise RuntimeError(f'group {self} does not have a full\
                               list of irreducible representations')
        return True

    def relations(self):
        '''
        Return the relations imposed on the underlying free group
        '''
        raise NotImplementedError()

    def irreducible_representations(self):
        '''
        Return a list of irreducible representations of this group
        '''
        raise NotImplementedError()

    def irreducible_representation(self, ind):
        '''
        Return a particular irreducible representation of this group
        '''
        return self.irreducible_representations()[ind]

    def fundamental_representation(self):
        '''
        Return the fundamental representation of this group
        '''
        raise NotImplementedError()
