'''
reflection groups...
'''

from sage.groups.free_group import FreeGroup
from sage.rings.qqbar import QQbar
from sage.matrix.matrix_space import MatrixSpace

from sage.functions.other import sqrt
from sage.rings.integer import Integer

from .representation import trivial_representation, representation_from_generators
from .simple_geometric_group import SimpleGeometricGroup

class ReflectionGroup(SimpleGeometricGroup):
    '''
    main class for reflection groups
    '''

    def __init__(self, kind):
        # TODO: check kind is well-formed
        # TODO: automatically translate known isos for speed reasons
        # (e.g. computing ccs reps for H4 as exterior tensors of reps
        # for H3 is massively faster)
        self._irreps = []
        self.kind = kind

        rank = int(kind[1:])
        F = FreeGroup(rank)
        I = []
        for i in range(rank):
            I.append(F.gen(i) ** 2)

        if kind[0] not in  ('D', 'E'):
            for i in range(rank):
                if kind[0] not in 'F' and i < rank - 2:
                    I.append((F.gen(i) * F.gen(i+1)) ** 3)
                for j in range(i-1):
                    I.append((F.gen(i) * F.gen(j)) ** 2)

        if rank > 1:
            if kind[0] == "A":
                I.append((F.gen(rank-2) * F.gen(rank-1)) ** 3)
            elif kind[0] == "B":
                I.append((F.gen(rank-2) * F.gen(rank-1)) ** 4)
            elif kind[0] == "G":
                I.append((F.gen(rank-2) * F.gen(rank-1)) ** 6)
            elif kind[0] == "H":
                I.append((F.gen(rank-2) * F.gen(rank-1)) ** 5)

        # TODO: logic for remaining group types
        if kind[0] in ('D', 'E', 'F'):
            raise NotImplementedError

        G = F / I
        super().__init__(G)

    def __repr__(self):
        return f'Order {self.G.order()} reflection group of type {self.kind}'

    # Compute and return the irreps of G over the ring R
    def irreducible_representations(self, R = QQbar):
        '''
        compute irreducible representations of a reflection group
        '''

        # Store previous computations for speed
        # TODO: this does not take into account the ring R
        if len(self._irreps) > 0:
            return self._irreps

        # TODO: Compute irreps of reflection groups here
        # Store result in self.irreps
        gens = self.gens()

        # every group admits the trivial representation
        self._irreps.append(trivial_representation(self, MatrixSpace(R,1)))

        # plus irreps from the generators
        MS = MatrixSpace(R, 1)
        # 'sign representation'
        # (coincides with normal sign rep on S_n = W(A_{n-1})
        self._irreps.append(representation_from_generators(
            self,
            { g : MS.matrix([-1]) for g in gens }))

        # for testing purposes, the 2-d irrep of W(A2) = S3 (i.e. the fundamental)
        # this is the representation acting permuting the vertices of an equilateral
        # triangle in the plane; so gens[0] = (12) is reflection along the x axis
        # while gens[1] = (23) is reflection through the line subtending angle 2pi/3
        # the normal to the reflecting hyperplane subtends angle pi/6, so
        # use formula refl(x) = x - 2<v,x> v, where v = exp(i pi/6) = ( cos(pi/6), sin(pi/6) ) = (sqrt(3)/2, 1/2)
        # thus (1,0) -> (1,0) - sqrt(3) (sqrt(3)/2, 1/2) = (-1/2, -sqrt(3)/2)
        # thus (0,1) -> (0,1) - (sqrt(3)/2, 1/2) = (-sqrt(3)/2, 1/2)
        if self.kind == "A2":
            #from sage.misc.functional import sqrt
            MS = MatrixSpace(R, 2)
            self._irreps.append(representation_from_generators(
                self,
                { gens[0] : MS.matrix([ [ 1, 0 ], [ 0, -1 ] ]),
                    # hey past spencer, what the heck is with the Integer here?
                    # great question, future spencer: Python likes to greedily optimize expressions like
                    # 1/2 before sage can get to it; we get a floating-point 0.5 rather than a symbolic
                    # 1/2. By casting to Integer, we ensure that all computations are done in Sage
                  gens[1] : MS.matrix([[-Integer(1)/2, -sqrt(3)/2], [-sqrt(3)/2, Integer(1)/2]]) }))

        super()._character_orthogonality_test(complete=True)

        return self._irreps
