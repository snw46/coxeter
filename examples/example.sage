from coxeter.reflection_group import ReflectionGroup

# Let's play around with reflection groups
G = ReflectionGroup("B3")

# Computing the order of a reflection group
print(f"Order of G: {G.order()}")

# Computing the representations of a reflection group (TODO)
print(f"Representations of G: {G.irreducible_representations()}")

# Characters:
# (Print in the format order  size:   trace)
print("Trivial rep:")
print(G.irreducible_representations()[0])
print("Sign rep:")
print(G.irreducible_representations()[1])

# Character inner product
print(f'Irreducible representations are orthogonal: {G.irreducible_representations()[0].dot(G.irreducible_representations()[1])} = 0')

# What's in a representation?
rho = G.irreducible_representations()[1]

r1, r2, r3 = G.gens()
print(f"Representation rho at r1 is {rho(r1)}")
print(f"Representations are homomorphisms: {rho(r1)*rho(r2)} = {rho(r1*r2)}")
