from coxeter.polytope import *
from coxeter.polyhedral_group import PolyhedralGroup

BI = PolyhedralGroup("H3")
fund = BI.irreducible_representations()[2]
P = OrbitPolytope(fund)
pp = P.plot()
pp.show()
