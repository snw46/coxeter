Introduction
============

``coxeter`` is a Sage package for performing simple representation theoretic computations with finite real reflection groups, their rotary subgroups, and double covers thereof.
This package is intended t

Features:
- Compute representations and character tables for supported group types.
- Use the BDT algorithm [1]_ to compute canonical forms and spaces of equivariant maps between representations.
- Realize group orbit polytopes using qhull and a robust :py:class:`coxeter.polytope.Polytope` class to visualize various projections.
- Generate McKay diagrams associated to an arbitrary group representation.
- Perform optimized computations on semisimple group types compared to GAP.
- Compute interior and exterior tensor products of representations.

.. [1] Bischer I., Döring C., and Trautner A. *Simultaneous Block Diagonalization of Matrices of Finite Order*. J. Phys. A: Math. Theor. 54 085203 (2021).
