from sage.groups.group import Group
from .representation import Representation

class DirectProductGroupElement():
    def __init__(self, _left, _right):
        self._left = _left
        self._right = _right

    def left(self):
        return self._left

    def right(self):
        return self._right

    def __repr__(self):
        return f'({self._left}, {self._right})'

    def __str__(self):
        return f'({self._left}, {self._right})'

    def __mul__(self, other):
        return DirectProductGroupElement(self.left() * other.left(), self.right() * other.right())

    def __hash__(self):
        from builtins import hash
        return hash((self.left().__hash__(), self.right().__hash__()))

    def __eq__(self, other):
        return self.left() == other.left() and self.right() == other.right()

    def inverse(self):
        return DirectProductGroupElement(self.left().inverse(), self.right().inverse())

    def order(self):
        from sage.arith.functions import lcm
        return lcm(self.left().order(), self.right().order())

class DirectProductGroup(Group):    
    def __init__(self, _left, _right):
        self._left = _left
        self._right = _right

        self._list = []

        self._conjugacy_classes = []

        self._irreps = []
 
        self.e = DirectProductGroupElement(self._left.e, self._right.e)

    def __repr__(self):
        return f'Direct product of {self.left()} and {self.right()}'

    def __str__(self):
        return f'Direct product of {self.left} and {self.right}'

    def left(self):
        return self._left

    def right(self):
        return self._right

    def order(self):
        return self._left.order() * self._right.order()

    def __getitem__(self, key):
        if self._list == []:
            for g in self._left:
                for h in self._right:
                    self._list.append(DirectProductGroupElement(g,h))

        return self._list[key]

    def conjugacy_classes(self):
        if self._conjugacy_classes != []:
            return self._conjugacy_classes

        # A quick hack; would be better if this integrated
        # more closely with the native Sage conjugacy class
        # structures
        class DirectProductConjugacyClass():
            def __init__(self, _representative, _size):
                self._representative = _representative
                self._size = _size

            def representative(self):
                return self._representative

            def __len__(self):
                return self._size

        for ccl in self.left().conjugacy_classes():
            for ccr in self.right().conjugacy_classes():
                self._conjugacy_classes.append(DirectProductConjugacyClass(
                        DirectProductGroupElement(ccl.representative(),
                                                  ccr.representative()),
                        len(ccl) * len(ccr)))

        return self._conjugacy_classes

    # construct an external tensor product representation of G, given
    # representations on the left and right factors of G
    def tensor(self, left_rep, right_rep):
        return Representation(self,
                              { g : left_rep(g.left()).tensor_product(right_rep(g.right())) for g in self })

    def irreps(self):
        if self._irreps != []:
            return self._irreps

        for left_rep in self._left.irreps():
            for right_rep in self._right.irreps():
                self._irreps.append(self.tensor(left_rep, right_rep))

        return self._irreps

