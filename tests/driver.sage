'''
This document serves as an example of the interfaces we might expect our 
code to satisfy, written as a collection of unit tests
Run with `sage driver.sage`
'''

import unittest

from coxeter.reflection_group import *
from coxeter.representation import *
from coxeter.polyhedral_group import *

class TestGroupOrder(unittest.TestCase):
    '''
    Ensure various groups have the expected order
    '''

    def test_group_order_A1(self):
        '''
        A1 should have order 2
        '''
        self.assertEqual(ReflectionGroup("A1").order(), 2,
                         'Group A2 has incorrect order')

    def test_group_order_A2(self):
        '''
        A2 should have order 6
        '''
        self.assertEqual(ReflectionGroup("A2").order(), 6,
                         'Group A2 has incorrect order')

    def test_group_order_G2(self):
        '''
        G2 should have order 12
        '''
        self.assertEqual(ReflectionGroup("G2").order(), 12,
                         'Group G2 has incorrect order')

    def test_group_order_A3(self):
        '''
        A3 should have order 24
        '''
        self.assertEqual(ReflectionGroup("A3").order(), 24,
                         'Group A3 has incorrect order')

    def test_group_order_B3(self):
        '''
        B3 should have order 48
        '''
        self.assertEqual(ReflectionGroup("B3").order(), 48,
                         'Group B3 has incorrect order')

    def test_group_order_H3(self):
        '''
        H3 should have order 120
        '''
        self.assertEqual(ReflectionGroup("H3").order(), 120,
                         'Group H3 has incorrect order')

    def test_group_order_H4(self):
        '''
        H4 should have order 14400
        '''
        self.assertEqual(ReflectionGroup("H4").order(), 14400,
                         'Group H4 has incorrect order')

    def test_group_order_H3_poly(self):
        '''
        binary icosahedral should have order 120
        '''
        self.assertEqual(PolyhedralGroup("H3").order(), 120,
                         'Group H3 (polyhedral) has incorrect order')


class ErrorTests(unittest.TestCase):
    '''
    Test that errors are handled gracefully
    '''

    def test_bad_key(self):
        '''
        Check for indexing groups with bad elements
        '''

        with self.assertRaises(KeyError):
            G = ReflectionGroup("H3")
            G['foo']

    def test_dot_representation_different_base(self):
        '''
        Check that representation inner product is only computable 
        on representations over the same base group
        '''

        with self.assertRaises(TypeError):
            G = PolyhedralGroup("H3")
            H = ReflectionGroup("A2")
            G.irreducible_representations()[0].dot(H.irreducible_representations()[0])

    def test_tensor_representation_different_base(self):
        '''
        Check that representation tensor product is only computable 
        on representations over the same base group
        '''

        with self.assertRaises(TypeError):
            G = PolyhedralGroup("H3")
            H = ReflectionGroup("A2")
            G.irreducible_representations()[0] * H.irreducible_representations()[0]

unittest.main()
