'''
this module implements methods related to the representation theory of finite groups
'''

from collections import deque

from sage.matrix.matrix_space import MatrixSpace
from sage.matrix.special import block_matrix, block_diagonal_matrix, identity_matrix
from sage.rings.qqbar import QQbar, AA
from sage.rings.cc import CC
from sage.rings.real_mpfr import RR
from sage.misc.functional import round

from .utils import Progress

class Representation():
    '''
    class to wrap a group G and a map from G to matrices in a given matrix space
    '''
    def __init__(self, G, m, base_ring):
        # on the backend, a finite group representation is a map
        # from elements of G to matrices
        self.G = G
        self.m = m
        self._base_ring = base_ring

    # Allow syntax like rho(g) for g in G.
    def __call__(self, g):
        try:
            h = self.G.reduce_word(g)
            if isinstance(self.m[h], tuple):
                g1, g2 = self.m[h]
                self.m[h] = self(g1) * self(g2)
            return self.m[h]
        except KeyError as exc:
            raise RuntimeError(f'word {g} had reduction {h} for \
                               which the representation of {self.G} was unknown') from exc


    def __repr__(self):
        return f'{self.dimension()} dimensional representation of {self.G}'

    def __str__(self):
        serial = []
        for cc in self.G.conjugacy_classes():
            serial.append(f'{cc.representative().order()} \
                          {(len(cc))}:\t{self(cc.representative()).trace().n()}')
        return '\n'.join(serial)

    def dot(self, other):
        '''
        compute representation inner product, an average of the inner product over the group
        use conjugacy class structure to speed up
        '''

        if self.G != other.G:
            raise TypeError(f'representation {self} and representation\
                            {other} have different base groups')
        return sum(len(cc)*self(cc.representative()).trace()*other(cc.representative()).trace()
                    for cc in self.G.conjugacy_classes()) / self.G.order()

    def base_ring(self):
        '''
        Return the ring over which the representation is defined
        '''
        return self._base_ring

    def dimension(self):
        '''
        compute the dimension of the representation as the trace of the identity map
        '''
        return self(self.G.e).ncols()

    def __mul__(self, other):
        '''
        the internal tensor product of A and B takes another representation on the **SAME** group
        and defines (A x B)(g) = A(g) x B(g) (Kronecker product of matrices)
        '''
        if self.G != other.G:
            raise TypeError(f'representation {self} and representation {other} have different\
                            base groups (did you mean to take an external tensor product?)')

        # there is a hierarchy of allowed rings; prefer complex over real,
        # and prefer numeric over symbolic thus take the highest of AA < RR < QQbar < CC found
        return Representation(self.G,
                { g : self(g).tensor_product(other(g),subdivide=False) for g in self.G },
                              base_ring=ring_tensor(self._base_ring, other._base_ring))

    def __add__(self, other):
        '''
        The direct sum representation
        '''
        return Representation(self.G,
                { g: block_diagonal_matrix([self(g), other(g)], subdivide=False) for g in self.G },
                              base_ring=ring_tensor(self._base_ring, other._base_ring))

    def _verify_relations(self, EPSILON=1e-9):
        '''
        ensure that the representation satisfies the relations
        '''
        for rel in self.G.relations():
            mat = self(self.G.e)
            for r in rel:
                if r < 0:
                    r *= -1
                    mat *= self(self.G.gen(r-1)).inverse()
                else:
                    mat *= self(self.G.gen(r-1)).inverse()
            if (mat - identity_matrix(self.dimension())).norm() > EPSILON:
                raise ValueError(f'In {repr(self)}, relation {rel} failed: \
                                 expected identity matrix, got {mat}')

def representation_from_generators(G, vs, base_ring=QQbar):
    '''
    define a representation on a FP group by vs a map of generators of G to the matrix space MS
    '''
    #pylint: disable=protected-access
    MS = MatrixSpace(base_ring, vs[G.gen(0)].ncols())

    vs = { g: MS.matrix(v) for (g,v) in vs.items() }

    for v in vs.values():
        if v.parent() != MS:
            raise TypeError('not all provided values belong to the same matrix space')

    # from sage.calculus.calculus import maxima as maxima_calculus
    # maxima_calculus('algebraic: true')
    # ^ this can be used to simplify_rational(), if doing so yields better results
    m = {}

    with Progress(f'compute {vs[G.gen(0)].ncols()}-diml rep') as bar:

        # word reducing BFS to compute representatives for everything
        q = deque()
        m[G.id()] = MS.identity_matrix()
        for g in G.gens():
            m[g] = vs[g]
            q.append(g)

        while q:
            bar.progress(float(len(m)) / G.order())
            g = q.popleft()

            # precondition: g is in standard form (i.e. g in list(G)) AND
            # self.m[g] is populated and simplified
            for gen in G.gens():
                h = G.reduce_word(g * gen)
                if h in m:
                    continue

                m[h] = (g, gen)
                q.append(h)

    rho = Representation(G, m, base_ring=base_ring)
    rho._verify_relations()
    return rho

def trivial_representation(G, MS = MatrixSpace(AA, 1)):
    '''
    return a Representation giving the trivial representation of G in the provided matrix space
    '''
    try:
        return Representation(G, { g: MS.matrix([1]) for g in G }, base_ring=MS.base_ring())
    except TypeError as exc:
        raise TypeError(f'representations are not implemented for \
                          non-iterable base group {G}') from exc

def ring_tensor(br1, br2):
    '''
    compute the 'least upper bound' of two rings, taking into account
    numerical precision and real/complex-ness.
    To be used when defining a tensor product representation; e.g.
    if one tensors a rep over CC and a rep over QQbar, the resulting
    rep must be over CC, because we cannot convert from approx to symbolic.
    '''
    is_complex = False
    is_numeric = False
    if br1 == CC or br1 == QQbar or br2 == CC or br2 == QQbar:
        is_complex = True
    if br1 == RR or br1 == CC or br2 == RR or br2 == CC:
        is_numeric = True
    return {
        (True, True): CC,
        (True, False): QQbar,
        (False, True): RR,
        (False, False): AA
    }[(is_complex, is_numeric)]

def canonical_transform(V, reps=None, _bar=None):
    '''
    put a representation V into canonical form
    pass a list reps of representations to override
    the default ordering of blocks
    we use the algorithm of Bischer Doring and Trautner (2020)
    If P is the output of this routine, then P.inverse() * V(g) * P
    will be in block form for all g
    '''
    if reps is None:
        reps = V.G.irreducible_representations()

    vec = []
    for i, rho in enumerate(reps):
        if _bar:
            _bar(i/len(reps))

        if V.dot(rho) == 0:
            continue

        M = block_matrix([ [rho(V.G.e).tensor_product(V(g)) -
                                rho(g).transpose().tensor_product(V(V.G.e))]
                            for g in V.G.gens() ], subdivide=False)
        for v in M.right_kernel().basis():
            vec.extend(list(v))

    MS = MatrixSpace(V.base_ring(), V.dimension())
    return MS.matrix(vec).transpose()


def compute_equivariant_map(V, W, _all=False, _verify=False):
    '''
    It is helpful, frequently to be able to compute a non-zero
    equivariant map between two representations. That is, a matrix
    M not equal to zero such that for every generator g of G (and thus
    for all g in G) it holds that M * V(g) = W(g) * M
    (It is an a priori requirement that if W is k x k and
    V is n x n then M must be k x n, and the LHS/RHS are k x n)
    This function computes such an equivariant map in the event it exists,
    or returns None otherwise
    '''
    if V.G is not W.G:
        raise TypeError(f'representations {repr(V)}, {repr(W)} have different base group')

    # if V, W are orthogonal there is no equivariant map between them
    if V.dot(W) == 0:
        return [] if _all else None

    # precompute irreps
    V.G.irreducible_representations()

    with Progress(f'computing equivariant maps...') as bar:
        # otherwise, we can select a single irreducible representation rho that is
        # a subrepresentation of both V and W. We put both V, W in canonical form
        # with the block corresponding to rho first, and use an identity matrix
        # transform
        bar.progress(0)
        irrep_list = None
        for i, rho in enumerate(V.G.irreducible_representations()):
            if V.dot(rho) != 0 and W.dot(rho) != 0:
                irrep_list = V.G.irreducible_representations()[i:] + \
                                V.G.irreducible_representations()[:i]
                break

        bar.progress(0.05)

        P, Q = canonical_transform(V, irrep_list, lambda x: bar.progress(0.1 + 0.2 * x)), \
               canonical_transform(W, irrep_list, lambda x: bar.progress(0.3 + 0.2*x))
        bar.progress(0.5)

        MS = MatrixSpace(ring_tensor(V.base_ring(), W.base_ring()), W.dimension(), V.dimension())
        if not _all:
            map = MS.matrix(lambda i, j: 1 if i == j and i < irrep_list[0].dimension() else 0)
            bar.progress(1)
            return Q * map * P.inverse()

        # now instead we should build every map...
        # to do this we need to break into blocks of multiplicity
        # if the multiplicity of a d-diml irrep is q, then we should add entries A.tensor(id(d))
        # where id(d) is the d-diml identity matrix and A is a qxq matrix with a 1 in exactly one
        # position and zeroes elsewhere. Then we should add this block matrix to a blank kxn matrix
        # in the correct spot and perform the similarity transform to get an invariant map

        # v_acc and w_acc are the total dimension accounted for by representations
        # occurring earlier in irrep_list than rho at the start of each iteration
        maps = []

        v_acc = 0
        w_acc = 0
        for i, rho in enumerate(irrep_list):
            bar.progress(0.5 + 0.5 * i/len(irrep_list))

            v_mul = round(V.dot(rho).n().real())
            w_mul = round(W.dot(rho).n().real())

            # space of maps corresponding to this rho is v_mul * w_mul
            if v_mul * w_mul == 0:
                continue

            for j in range(v_mul):
                for k in range(w_mul):
                    map = [ [ 0 for _ in range(V.dimension()) ] for _ in range(W.dimension()) ]
                    # add a rho.dimension() identity matrix to map at the point (w_acc + rho.dim * k, v_acc + rho.dim * j)
                    for l in range(rho.dimension()):
                        map[w_acc + k * rho.dimension() + l][v_acc + j * rho.dimension() + l] += 1

                    maps.append(Q * MS.matrix(map) * P.inverse())

            v_acc += rho.dimension() * v_mul
            w_acc += rho.dimension() * w_mul

        if _verify:
            for map in maps:
                for g in V.G.gens():
                    err = (map * V(g) - W(g) * map).norm()
                    if err > 0.001:
                        raise ValueError(f'Could not verify equivariance; on generator {g} got ' \
                                        f'error of {err} for map {map}')

    return maps

def compute_equivariant_maps(V, W, _verify=False):
    '''
    As compute_equivariant_map, but compute all maps
    '''
    return compute_equivariant_map(V, W, _all=True, _verify=_verify)
